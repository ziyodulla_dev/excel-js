class Dom {
  constructor(selector) {
    this.$el = typeof selector === 'string' ? document.querySelector(selector) : selector;
  }

  html(html) {
    if (typeof html === 'string') {
      this.$el.innerHTML = html;
      return this;
    }
    return this.$el.outerHTML.trim();
  }

  on(eventType, fn) {
    this.$el.addEventListener(eventType, fn);
  }

  off(eventType, fn) {
    this.$el.removeEventListener(eventType, fn);
  }

  clear() {
    this.html('');
    return this;
  }
}

export function $(selector) {
  return new Dom(selector);
}

$.create = (tag, classes = '') => {
  const el = document.createElement(tag);
  if (classes) el.classList.add(classes);
  return el;
};